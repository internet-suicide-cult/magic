# Composite Magic

# 現代人のための魔術の構成

1. [いかなる者がいかなる魔術を行使し得るか](spirits_and_their_magics.md)
1. [死者が生前に受けた苦痛を遡及的に取り除く術](mourning.md)
1. [生者を使役する術](force.md)
